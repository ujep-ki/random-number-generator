package io.techniktom;

import java.io.*;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("generated_numbers.txt");
        long from;
        long to;
        long count;

        BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

        try {
            System.out.print("Enter Minimum value: ");
            String fromStringValue = consoleReader.readLine();
            from = Long.parseLong(fromStringValue);
        } catch (NumberFormatException e) {
            System.err.println("You did not entered number!");

            return;
        }

        try {
            System.out.print("Enter Maximux value: ");
            String toStringValue = consoleReader.readLine();
            to = Long.parseLong(toStringValue);
        } catch (NumberFormatException e) {
            System.err.println("You did not entered number!");

            return;
        }

        if(from >= to) {
            System.err.println("You enetered nonsence Minimum and Maximum numbers!");

            return;
        }

        try {
            System.out.print("Enter Count of nubers: ");
            String countStringValue = consoleReader.readLine();
            count = Long.parseLong(countStringValue);
        } catch (NumberFormatException e) {
            System.err.println("You did not entered number!");

            return;
        }

        if(file.exists()) {
            file.delete();
        }

        long serialStartTime = System.currentTimeMillis();

        Long[] serialArray = generateNumbers(from, to, count);

        long serialEndTime = System.currentTimeMillis();

        long parallelStartTime = System.currentTimeMillis();

        Long[] parallelArray = generateNumbersParallel(from, to, count);

        long parallelEndTime = System.currentTimeMillis();

        System.out.println("Generating numbers taken: " + (serialEndTime - serialStartTime) + "ms");

        System.out.println("Generation rate (serial): " + ((float) serialArray.length)/((float)(serialEndTime - serialStartTime)));

        System.out.println("Generating numbers taken: " + (parallelEndTime - parallelStartTime) + "ms");

        System.out.println("Generation rate (serial): " + ((float) parallelArray.length)/((float)(parallelEndTime - parallelStartTime)));

        try (PrintWriter writer = new PrintWriter(new FileWriter(file))) {
            Arrays
                .stream(serialArray)
                .forEach(writer::println);



            writer.flush();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static String formatNumber(int counter) {
        StringBuilder sb = new StringBuilder();
        char[] stringNumber = String.valueOf(counter).toCharArray();

        for (int i = stringNumber.length - 1; i >= 0; i--) {
            int position = stringNumber.length - 1 - i;

            sb.append(stringNumber[i]);

            if(position % 3 == 0 && position != 0 && i != 0) {
                sb.append(' ');
            }
        }

        return sb.toString();
    }

    private static Long[] generateNumbersParallel(Long from, Long to, Long count) {
        Long[] array = new Long[count.intValue()];
        long range = to - from;

        IntStream
            .range(0, array.length)
            .parallel()
            .forEach(i -> array[i] = (from + Math.round(Math.random() * range)));

        return array;
    }

    private static Long[] generateNumbers(Long from, Long to, Long count) {
        Long[] array = new Long[count.intValue()];
        long range = to - from;

        for (int i = 0; i < array.length; i++) {
            //System.out.println(Thread.currentThread().getId());
            array[i] = (from + Math.round(Math.random() * range));
        }

        return array;
    }
}
